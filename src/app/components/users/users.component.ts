import { Component, OnInit } from '@angular/core';
import { FormControl, FormBuilder, Validators, FormGroup, NgForm } from '@angular/forms';
import { UserService } from 'src/app/shared/users.service';
import { User } from './user';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';
import { AngularFirestore } from '@angular/fire/firestore';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss']
})

export class UsersComponent implements OnInit {

  userForm = new FormGroup({
    title: new FormControl('', [Validators.required, Validators.minLength(3)])
  });
  title: {};
  user: User;
  usersList: Observable<User[]>;
  showModal = false;
  users = [];
  filteredUsers: any;

  constructor(private service: UserService, private fb: FormBuilder, private router: Router, private firestore: AngularFirestore) {

  }
    /// Active filter rules
    filters = {};
    filterPost = '';

  ngOnInit() {
    this.reloadData();
  }

  onSubmit() {
    this.postUser(this.user);
  }

  handleNewTicket() {
    this.showModal = true;
  }
  closeModal() {
    this.showModal = false;
  }
  postUser(user) {
    if (!this.userForm.value.title) {
      return;
    }
    const { title } = this.userForm.value;
    this.firestore.collection('users').add({ title });
    this.showModal = false;
    this.reloadData();
  }
  reloadData() {
    this.service.getUsers().subscribe(actionArray => {
      this.users = actionArray.map(item => {
        return {
          id: item.payload.doc.id,
          ...item.payload.doc.data()
        } as User;
      });
    });

  }
  suppressUser(id: any) {
    if (confirm('Are you sure to delete this item?')) {
      this.service.deleteUser(id).subscribe(
        data => {
          this.reloadData();
        }
      );
    }
  }
  resetForm(user?: NgForm) {
    if (user != null) {
      user.resetForm();
    }
    this.service.user = {
      id: null,
      title: ''
    };
  }
  updateUser(user: User) {
    this.router.navigate(['/users', user.id]);
  }
  onDelete(id: string): void {
    if (confirm('Are you sure to delete this record?')) {
       this.firestore.collection('users').doc(id).delete();
    }
  }

}
