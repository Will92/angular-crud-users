import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/shared/users.service';
import { User } from '../user';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { AngularFirestore } from '@angular/fire/firestore';

@Component({
  selector: 'app-update-user',
  templateUrl: './update-user.component.html',
  styleUrls: ['./update-user.component.sass']
})
export class UpdateUserComponent implements OnInit {

  users: [];
  editForm: FormGroup;
  first: any;
  id: any;
  title: [];
  public user = {} as User;

  constructor(private formBuilder: FormBuilder,
    private service: UserService,
    private router: Router,
    private route: ActivatedRoute,
    private firestore: AngularFirestore) { }

  ngOnInit(): void {
    this.editForm = new FormGroup({
      id: new FormControl(''),
      title: new FormControl('', [Validators.required, Validators.minLength(3)])
    });
    this.getUserById(this.id);
  }
  getUserById(id: any): void {
    this.route.params.forEach((params: Params) => {
      if (params.id !== undefined) {
        const id = params.id;
        const userResult = this.firestore.collection('users').doc(id).snapshotChanges();
        const editSubscribe = userResult.subscribe((user) => {
          const userEdit = {
            id,
            title: user.payload.data()[ 'title' ]
          };
          this.editForm.setValue(userEdit);
          this.user = userEdit;
          editSubscribe.unsubscribe();
        });
      }
    });
  }
  onSubmit(user): void {
    const { title, id } = this.editForm.value;
    this.firestore.collection('users').doc(id).set({ title });
    console.warn(this.editForm, 'editForm editado');
    this.router.navigate(['/users']);
  }
}
