import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { User } from '../components/users/user';
import { HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { AngularFirestore } from '@angular/fire/firestore';


@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private http: HttpClient,  private firestore: AngularFirestore) { }
  private url = 'http://localhost:3000/users';

  user: User;
  users: User[] = [];
  form = new FormGroup({
    id: new FormControl(null),
    title: new FormControl('', Validators.required)
  });
  getUsers() {
    return this.firestore.collection('users').snapshotChanges();
  }

  getUserById(id: number): Observable<User> {
    return this.http.get<User>(this.url + '/' + id);
  }
  public postUser(user: {title: string}) {
    return this.firestore.collection('users').add(this.user);
  }
  deleteUser(id): Observable<{}> {
    return this.http.delete(`${this.url}/${id}`, { responseType: 'text' });
  }

  populateForm(user) {
    this.form.setValue(user);
  }

  public putUser(id: string, user: any) {
    return this.firestore.collection('users').doc(id).set(user);
  }

  searchTitle(termino: string) {
    const nombreArr: User[] = [];

    for (const user of this.users) {
      const title: string = user.title.toLocaleLowerCase();
      if (title.indexOf(termino) !== 1) {
        nombreArr.push(user);
      }
    }

  }
  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error); // for demo purposes only
    return Promise.reject(error.message || error);
 }
}
